﻿using System;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewModdingAPI.Utilities;
using StardewValley;
using System.IO;

namespace RealisticCrops
{
    public partial class ModEntry
    {
        private class ModConfig
        {
            public string[] SpreadingPlants { get; set; }

            public ModConfig()
            {
                SpreadingPlants = new string[] {
                    "jazz",
                    "garlic",   
                    "strawberry",
                    "tulip",
                    "blueberry",
                    "melons",
                    "poppy",
                    "summer spangles",
                    "sunflower",
                    "amaranth",
                    "cranberries",
                    "pumpkin",
                    "fairy rose"
                };
            }
        }
    }
}